package org.fasttrackit;

import org.fasttrackit.employee.CLvlEmployee;
import org.fasttrackit.employee.Employee;
import org.fasttrackit.output.AtmDispatcher;
import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.Person;

import static org.fasttrackit.UserActions.*;
import static org.fasttrackit.input.UserInput.authenticate;
import static org.fasttrackit.utils.DataUtils.getCustomer;

public class AtmApp {
    public static final String ATM_NAME = "Free Cash ATM";
    public static void main(String[] args) {
        BankCustomer someOne = getCustomer();

        Display.showWelcomemsg();
        boolean passThrough = authenticate(someOne);
        if (!passThrough) {
            Display.lockUserFor24h();
            return;
        }

        Display.showMenu();
        String option = someOne.selectOptionMenu();
        switch (option) {
            case "0" -> Display.repairAtm(initCLvlEmployee());
            case "1" -> withdraw(someOne);
            case "2" -> Display.showCustomerBalanceSheet(someOne.getBankAccount());
            case "3" -> changePin(someOne);
            case "4" -> Display.activateCardMessage(someOne);
            case "5" -> depositCash(someOne);
            case "6" -> deactivateAccount(someOne);
        }
    }

    private static Employee initEmployee() {
        return new Employee("1860601037891","Toni","Ton","Mr.");
    }

    private static CLvlEmployee initCLvlEmployee() {
        return new CLvlEmployee("1860601037892","Soni","Son","Mr.", "CEO");
    }
}