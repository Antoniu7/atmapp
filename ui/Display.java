package org.fasttrackit.ui;

import org.fasttrackit.employee.Employee;
import org.fasttrackit.input.UserInput;
import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;

import static org.fasttrackit.AtmApp.ATM_NAME;
import static org.fasttrackit.input.UserInput.readFromKeyboard;

public class Display {

    public static final String APP_MENU =
            """
                    Select an option:
                    1. Withdraw cash
                    2. Check balance
                    3. Change pin
                    4. Activate card
                    5. Deposite cash
                    6. Deactivate account
                     """;

    public static void showWelcomemsg() {
        System.out.println("Welcome to " + ATM_NAME);
    }

    public static String askForPin() {
        System.out.println("Please enter your pin number:");
        return readFromKeyboard();
    }
    public static String askForNewPin() {
        System.out.println("Please enter your new pin number:");
        return readFromKeyboard();
    }

    public static String confirmNewPin() {
        System.out.println("Please confirm new pin number:");
        return readFromKeyboard();
    }

    private static String readFromKeyboard() {
        return UserInput.readFromKeyboard();
    }
    public static void displayInvalidPinMsg() {
        System.out.println("Pin incorrect, please try again.");
    }

    public static void lockUserFor24h() {
        System.out.println("Incorrect pin, card locked for 24h.");
    }

    public static void showMenu() {
        System.out.println(APP_MENU);
    }

    public static int askUserForAmount() {
        System.out.println("Type in amount you want: ");
        return UserInput.readIntFromKeyboard();
    }

    public static void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your balance is: " + bankAccount.getBalance() + " " + bankAccount.getCurrency());
    }

    public static void showNotEnoughMoneyMsg() {
        System.out.println("Your bank account does not have enough cash.");
    }

    public static void activateCardMessage(BankCustomer someOne) {
        System.out.printf("Thank you %s for activating the card. ",someOne.getFullName());
    }

    public static void informUserWithDeactivation() {
        System.out.println("Thank you for being a valuable customer. Hope you come back.");
    }

    public static void repairAtm(Employee employee) {
        System.out.println("Employee " + employee.getFullName() + " is repairing the ATM.");
    }
}
