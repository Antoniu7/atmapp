package org.fasttrackit.user;

import org.fasttrackit.input.UserInput;

public class BankCustomer extends Person {
    private final int id;
    private BankAccount bankAccount;
    private boolean isActive;

    public BankCustomer(int bId, String cnp, String firstName, String lastName) {
        super(cnp, firstName, lastName);
        this.id = bId;
        this .isActive = true;
    }

    public void deactivateAccount() {
        this.isActive = false;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public boolean validatePin(String input) {
        return bankAccount.getCard().verifyPin(input);
    }

    public void updatePin(String changedPin) {
        bankAccount.getCard().updatePin(changedPin);

    }

    public String selectOptionMenu() {
        String option = UserInput.readFromKeyboard();
        System.out.println("Person: " + getFirstName() + " selected: " + option);
        return option;
    }

    public boolean validateChangedPin(String changedPin, String confirmedChangedPin) {
        return changedPin.equals(confirmedChangedPin);
    }
    @Override
    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public void depositCash(int amount) {
        double balance = bankAccount.getBalance();
        bankAccount.updateBalance(balance + amount);
    }
}
